﻿using Newtonsoft.Json.Linq;
using System;

namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This abstract the way we obtain Json data.
    /// </summary>
    public interface IJsonSource : IDisposable
    {
        /// <summary>
        /// Returns the raw JSON data contained in the raw data source.
        /// </summary>
        /// <returns>The JObject read from the data</returns>
        JObject GetDocument();
    }
}
