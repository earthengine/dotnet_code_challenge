﻿namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This is an abstract data type that we use in this challenge.
    /// The underneath implementation can have other fields, but
    /// we require the name and the price.
    /// 
    /// We could also introduce a ID field to identify its source.
    /// </summary>
    public interface IHorse
    {
        /// <summary>
        /// The name of the horse
        /// </summary>
        string Name { get; }
        /// <summary>
        /// The price of the horse
        /// </summary>
        decimal Price { get; }
    }
}
