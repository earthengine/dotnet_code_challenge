﻿using System;
using System.Xml;

namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This abstract the way we obtain XML data
    /// </summary>
    public interface IXmlSource : IDisposable
    {
        /// <summary>
        /// Returns the raw XML data contained in the raw data source.
        /// </summary>
        /// <returns>The XmlDocument read from the data</returns>
        XmlDocument GetDocument();
    }
}
