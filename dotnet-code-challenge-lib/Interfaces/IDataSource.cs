﻿using System.Collections.Generic;
using System.Linq;

namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// Abstract interface for obtaining data.
    /// 
    /// The underneath implementation can be a web service,
    /// a database or a file.
    /// </summary>
    public interface IDataSource
    {
        /// <summary>
        /// A data source returns some data it owns. Here we own some IHorse(s).
        /// </summary>
        /// <returns>The list of IHorse we own.</returns>
        IEnumerable<IHorse> Horses();
    }
}
