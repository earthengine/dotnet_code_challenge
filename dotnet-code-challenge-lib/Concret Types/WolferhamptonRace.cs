﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This data source is in Wolferhampton race format.
    /// </summary>
    public class WolferhamptonRace : IDataSource
    {
        private class WolferhamptonRaceHorse : IHorse
        {
            public string Name { get; set; }

            public decimal Price { get; set; }

            public string Id { get; set; }
        }
        private JObject doc;

        /// <summary>
        /// Create a data source by reading the Json format data.
        /// 
        /// It disposes its input, so the user should not use
        /// the supplied IJsonSource any more.
        /// </summary>
        /// <param name="json">The Json data source</param>
        public WolferhamptonRace(IJsonSource json) {
            using (json)
            {
                doc = json.GetDocument();
            }
        }

        public IEnumerable<IHorse> Horses()
        {
            var market_val = doc["RawData"]["Markets"];
            if (market_val == null) return Enumerable.Empty<IHorse>();
            return market_val.SelectMany(market => {
                return market["Selections"];
            }).Select(selection => {
                var tags = selection["Tags"];
                var price = selection["Price"];
                return new WolferhamptonRaceHorse
                {
                    Name = tags["name"].ToString(),
                    Id = tags["participant"].ToString(),
                    Price = decimal.Parse(price.ToString())
                };
            });
                                  
        }
    }
}
