﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;


namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This data source is in Caulfield race format.
    /// </summary>
    public class CaulfieldRace : IDataSource
    {
        private class CaulfieldRaceHorse : IHorse
        {
            public string Name { get; set; }

            public decimal Price { get; set; }

            public string Id { get; set; }
            public int Number { get; set; }

            public int RaceNumber { get; set; }
            public string RaceId { get; set; }
        }
        private XmlDocument doc;

        /// <summary>
        /// Create a data source by reading the XML format data.
        /// 
        /// It disposes its input, so the user should not use
        /// the supplied IJsonSource any more.
        /// </summary>
        /// <param name="xml">The XML data source</param>
        public CaulfieldRace(IXmlSource xml)
        {
            using (xml)
            {
                doc = xml.GetDocument();
            }
        }

        public IEnumerable<IHorse> Horses()
        {
            return FindHorsesInNode(doc).Select(horse =>
            {
                horse.Price = FindHorsePrice(horse);
                return horse;
            });
        }
        private IEnumerable<CaulfieldRaceHorse> FindHorsesInNode(XmlNode node) {
            return node.SelectNodes("/meeting/races/race")
                .Cast<XmlElement>()
                .SelectMany(race_elem =>
                {
                    var race_id = race_elem.GetAttribute("id");
                    var race_number = int.Parse(race_elem.GetAttribute("number"));

                    return race_elem.SelectNodes("horses/horse")
                           .Cast<XmlElement>()
                           .Select(horse_elem =>
                           {
                               var id = horse_elem.GetAttribute("id");
                               var name = horse_elem.GetAttribute("name");

                               var number = horse_elem.ChildNodes.Cast<XmlNode>()
                                                .Where(number_elem => number_elem.NodeType == XmlNodeType.Element
                                                            && number_elem.Name == "number")
                                                .Cast<XmlElement>()
                                                .Select(number_elem => int.Parse(number_elem.InnerText))
                                                .Single();

                               return new CaulfieldRaceHorse
                               {
                                   Id = id,
                                   Name = name,
                                   Number = number,
                                   RaceId = race_id,
                                   RaceNumber = race_number
                               };
                           });
                });
        }

        private decimal FindHorsePrice(CaulfieldRaceHorse horse)
        {
            var horse_price = doc.SelectNodes(
                $"/meeting/races/race/prices/price/horses/horse[@number=\"{horse.Number}\"]");
            return horse_price.Cast<XmlElement>()
                                   .Select(horse_node =>
                                        decimal.Parse(horse_node.GetAttribute("Price")))
                                   .Single();
        }
    }
}
