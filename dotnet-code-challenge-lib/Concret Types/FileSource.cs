﻿using System.IO;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace dotnet_code_challenge_lib
{
    /// <summary>
    /// This class implements IJsonSource and IXmlSource by reading a file.
    /// 
    /// We could think of WebSource or DbSource which uses Web or a database 
    /// as the data source.
    /// </summary>
    public class FileSource : IJsonSource, IXmlSource
    {
        private readonly StreamReader file;

        /// <summary>
        /// Open the given file and use as the source of data.
        /// </summary>
        /// <param name="file_path">The path of the file.</param>
        public FileSource(string file_path)
        {
            file = File.OpenText(file_path);
        }

        public void Dispose()
        {
            file.Dispose();
        }

        /// <summary>
        /// Read the file data as a Json object.
        /// </summary>
        /// <returns>The Json object read from the file.</returns>
        public JObject GetDocument()
        {
            return JObject.Load(new JsonTextReader(file));
        }

        /// <summary>
        /// Read the file data as Xml object.
        /// </summary>
        /// <returns>The XML document read from the file.</returns>
        XmlDocument IXmlSource.GetDocument()
        {
            var doc = new XmlDocument();
            doc.Load(file.BaseStream);
            return doc;
        }
    }
}
