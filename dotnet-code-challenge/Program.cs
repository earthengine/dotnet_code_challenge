﻿using dotnet_code_challenge_lib;
using System;
using System.Linq;
using System.Collections.Generic;

namespace dotnet_code_challenge
{
    class Program
    {
        // DataSources are lines of the format:
        // 
        // <IDataSource class name>, <IJsonSource or IXmlSource class name>=<connection specific data>
        //
        // For FileSource, the "connection specific data" is just a file path.
        static IDataSource ParseDataSource(string datasource)
        {
            var setting = datasource.Split(',');
            var rawsrc = setting[1].Split("=");
            if(rawsrc[0].Trim()!=typeof(FileSource).Name)
            {
                throw new Exception($"Unsupported raw source: {rawsrc[0]}");
            }
            var fs = new FileSource(rawsrc[1].Trim());
            if (setting[0].Trim() == typeof(CaulfieldRace).Name)
            {
                return new CaulfieldRace(fs);
            } else if (setting[0].Trim() == typeof(WolferhamptonRace).Name)
            {
                return new WolferhamptonRace(fs);
            }
            throw new Exception($"Unsupported data source: {setting[0]}");
        }

        static void Main(string[] args)
        {
            var horses = new SortedDictionary<decimal, List<IHorse>>();
            // Take all horses from configured data sources
            foreach (var datasource in dotnet_code_challenge.Default.DataSources)
            {
                var ds = ParseDataSource(datasource);
                foreach(var horse in ds.Horses())
                {
                    if(horses.ContainsKey(horse.Price))
                    {
                        horses[horse.Price].Add(horse);
                    } else
                    {
                        horses[horse.Price] = new List<IHorse>
                        {
                            horse
                        };
                    }
                }
            }

            // Output the horses in order.
            //
            foreach(var horse in horses.Values.SelectMany(l => l)) {
                Console.WriteLine($"horse name: {horse.Name} price: {horse.Price}");
            }
        }
    }
}
