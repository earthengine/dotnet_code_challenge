using Xunit;
using dotnet_code_challenge_lib;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace dotnet_code_challenge.Test
{
    public class UnitTest1
    {
        public class MockHourse : IHorse
        {
            public string Name { get; set; }

            public decimal Price { get; set; }
        }
        class MockDataSource : IDataSource
        {
            public IEnumerable<IHorse> Horses()
            {
                var rand = new Random();
                for (var i = 0; i < 10; ++i)
                {
                    yield return new MockHourse
                    {
                        Name = $"Hourse {i}",
                        Price = (decimal)((int)(rand.NextDouble() * 10000)) / 100
                    };
                }
            }
        }
        [Fact]
        public void Test_MockHourses()
        {
            var data = new MockDataSource();
            var hourses = data.Horses().OrderBy(horse => horse.Price).ToArray();
            Assert.InRange(hourses[3].Price, hourses[2].Price, hourses[6].Price);
        }

        public class MockXmlSource : IXmlSource
        {
            public void Dispose()
            {
                //Mock only; do nothing
            }

            public XmlDocument GetDocument()
            {
                var doc = new XmlDocument();
                doc.LoadXml(@"<meeting><races>
<race number=""1"">
<horses>
    <horse name=""horse1"" id=""0""><number>1</number></horse>
    <horse name=""horse2"" id=""1""><number>2</number></horse>
</horses>
<prices>
<price>
<horses>
    <horse number=""1"" Price=""3.1""/>
    <horse number=""2"" Price=""1.1""/>
</horses>
</price>
</prices>
</race>
</races></meeting>
");
                return doc;
            }
        }

        [Fact]
        public void Test_CaulfieldRace()
        {
            var xs = new MockXmlSource();
            var cr = new CaulfieldRace(xs);
            var horses = cr.Horses().ToArray();
            Assert.Equal(3.1m, horses[0].Price);
            Assert.Equal(1.1m, horses[1].Price);
        }

        public class MockJsonSource : IJsonSource
        {
            public void Dispose()
            {
                //Mock only; do nothing
            }

            public JObject GetDocument()
            {
                var json = @"
{
""RawData"": {
    ""Markets"": [
    {
        ""Selections"": [
        {""Price"": 10.1, ""Tags"": {
                ""participant"": ""1"",
                ""name"": ""Selection1""
            }
        },
        {""Price"": 4.1, ""Tags"": {
                ""participant"": ""2"",
                ""name"": ""Selection2""
            }
        }
        ]
    }]
}
}
";
                return JObject.Parse(json);
            }
        }

        [Fact]
        public void Test_WolferhamptonRace()
        {
            var src = new WolferhamptonRace(new MockJsonSource());
            var horses = src.Horses().ToArray();

            Assert.Equal(10.1m, horses[0].Price);
            Assert.Equal(4.1m, horses[1].Price);
        }
    }
}
